//Require express
const express = require('express');

//Initialize Router
const router = express.Router();

const AuthController = require('./../controllers/AuthController');

// /api/auth/login
router.post('/login', AuthController.login);

// /api/auth/logout
router.post('/logout', AuthController.logout);

module.exports = router