//Require express
const express = require('express');

//Initialize Router
const router = express.Router();

const TestController = require('./../controllers/TestController');

// middleware that is specific to this router
router.use((req, res, next) => {
  console.log('Time: ', Date.now());
  
  if(req.headers.authorization != process.env.API_KEY) {
    return res.status(401).json({message: "Unauthorized"});
  }

  next();
})


// define the home page route
router.get('/test', TestController.index);

// define the about route
router.get('/test/two', TestController.detail);

module.exports = router