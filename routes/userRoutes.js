//Require express
const express = require('express');
const jwt = require('jsonwebtoken');
//Initialize Router
const router = express.Router();

const UserController = require('./../controllers/UserController');

//JWT Middleware
router.use(async (req, res, next) => {
    let authToken = req.headers.authorization;

    if(!authToken) {
        return res.status(401).json({err: 'Not Allowed'});
    }

    //split the token by the space
    let tokenSplit = authToken.split(' ');

    let token = tokenSplit[1];

    if(!token) {
        return res.status(401).json({err: 'Not Allowed'});
    }

    try {
        var userInToken = await jwt.verify(token, process.env.JWT_SECRET);
    } catch (err) {
        return res.status(401).json({err: 'Not Allowed'});
    }

    console.log(userInToken);

    next();
});
  

// /api/auth/login
router.post('/create', UserController.create);

module.exports = router