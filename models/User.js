var mongoose = require('mongoose');

var crypto = require('crypto');

var userSchema = new mongoose.Schema({
    email: {type: String, unique: true},
    password: String,
    salt: String
}, {timestamps: true});

userSchema.pre('save', function(next) {
    if (this.password) {
        this.salt = new Buffer.from(
          crypto.randomBytes(16).toString('base64'), 
          'base64'
        );
        this.password = crypto.pbkdf2Sync(
            this.password, this.salt, 10000, 64, 'sha512').toString('base64');
    }
    next();
});

module.exports = mongoose.model('User', userSchema, 'users');

//public
module.exports.createUser = function(email, password) {
    return this.create({email, password});
}

module.exports.findUserByEmail = function(email) {
    return this.findOne({email: email});
}