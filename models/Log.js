const mongoose = require('mongoose');

let logSchema = new mongoose.Schema({
    path: {
        type: String,
        required: true
    },
    method: {
        type: String,
        upper: true,

        enum: ['GET', 'POST', 'PUT', 'DELETE']
    },
    request: {
        type: Object,
        default: {}
    },
    response: {
        type: Object,
        default: {}
    },
    code: {
        type: Number,
        default: 0
    },
    message: {
        type: String,
        required: true,
        trim: true
    },
    stack: String
}, {timestamps: true});

module.exports = mongoose.model('Log', logSchema, 'logs');