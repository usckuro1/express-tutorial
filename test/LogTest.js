
const assert = require('assert');

describe("Populat Base de datos", function() {
    before({
        //Seeder de la BD
    })
});

//Historia de Usuario
describe("Como sistema quiero almacenar las peticiones realizadas en la aplicacion para tener informacion a la mano", function() {
    it("Almacena Informacion", function() {
        let logs = {
            _id: 1,
            path: "/",
            request: {},
            user: {phone: '6642182508', role: 'admin'},
            response: { success: true, message: "Success, everything was OK!" }
        };

        //..procesa informacion

        return assert.notEqual(logs.user.role, 'admin');
    });
});

describe("Como administrador quiero poder eliminar usuarios del sistema", function (){
    it("Eliminar Usuario", function(){
        //codigo eliminar usuario
        return true;
    });

    it("Error de Usuario ya Eliminado", function(){
        return true;
    });

    it("Error por eliminar usuario administrador", function() {
        return true;
    });

    it("Error eliminarse a si mismo", function() {
        return true;
    });
})