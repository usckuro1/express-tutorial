const jwt = require('jsonwebtoken');
const User = require('./../models/User');
const crypto = require('crypto');

module.exports.login = async function(req, res, next) {
    try {
        //Find User
        let user = await User.findUserByEmail(req.body.email);

        //validate if user comes in the query
        if(!user) {
            res.status(401).json({err: 'Not Allowed'});
            next();
            return;
        }

        let salt = user.salt;

        //encrypt password the same as user schema
        let passwordEncrypted = crypto.pbkdf2Sync(
            req.body.password, salt, 10000, 64, 'sha512').toString('base64');

        //compares encrypted passwords
        if(passwordEncrypted != user.password) {
            res.status(401).json({err: 'Not Allowed'});
            next();
            return;
        }

        //generates token
        let token = await jwt.sign(JSON.stringify(user), process.env.JWT_SECRET);

        res.json({token: token, user: user});
    }catch(err) {
        res.status(400).json({err: err.message});
    }
   
    next();
};

module.exports.logout = async function(req, res, next) {
    next();
};