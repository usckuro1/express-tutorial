const UnauthorizedError = require('./../exceptions/UnauthorizedError');

module.exports.index = (req, res, next) => {
    try {
        let api_key = req.headers.authorization;

        if(req.query.complemento != 'SECRET') {
            throw new UnauthorizedError("Firma no Coincide", 4001);
        }

        res.json({success: true});

        next();

    } catch(err) {

        if(err instanceof UnauthorizedError) {
            return res.status(err.statusCode).json({error: err.message, code: err.errorCode, name: err.name});
        }

        res.status(500).json({error: "Ha Ocurrido un Error"});

        next();
    }
};

module.exports.detail = (req, res, next) => {
    res.send('About birds');
}