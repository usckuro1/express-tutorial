const User = require('./../models/User');

module.exports.create = async function(req, res, next) {
    try {
        let user = await User.create(req.body);
        res.json(user);
    } catch (err) {
        res.status(400).json({error: err.message});
    }
   
    next();
};
