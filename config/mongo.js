//require mongoose module
var mongoose = require('mongoose');

mongoose.connect(process.env.MONGO_URI, {useNewUrlParser: true});

mongoose.connection.on('connected', () => {
    console.log('Mongoose default connection is open to');
});

mongoose.connection.on('error', (err) => {
    console.log('Mongoose default connection has occured '+err+' error');
});

mongoose.connection.on('disconnected', () => {
    console.log(('Mongoose default connection is disconnected'));
});

process.on('SIGINT', () => {
    mongoose.connection.close(() => {
        console.log(('Mongoose default connection is disconnected due to application termination'));
        process.exit(0);
    });
});

module.exports = mongoose;