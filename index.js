//requiere express
const express = require('express');
var bodyParser = require('body-parser');
//Env File
require('dotenv').config();

//inicializa express
const app = express();
//port
const port = process.env.PORT;

require('./config/mongo');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

const testRoutes = require('./routes/testRoutes');
const authRoutes = require('./routes/authRoutes');
const userRoutes = require('./routes/userRoutes');

// app.use('/api', testRoutes);
app.use('/api/auth', authRoutes);
app.use('/api/user', userRoutes);

//initialize app
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})