module.exports = class UnauthorizedError extends Error {
    constructor(message, code) {
        super(message);
        this.name = 'UnauthorizedError';
        this.statusCode = 401;
        this.errorCode = code;
    }
}